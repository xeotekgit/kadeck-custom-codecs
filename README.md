# Kadeck Custom Codecs

A demo project demonstrating how to implement a custom codec for [Xeotek Kadeck](https://www.kadeck.com "Xeotek Kadeck Website").

Xeotek Kadeck is a monitoring and management UI for Apache Kafka and Amazon Kinesis.

Custom codecs are a powerful feature in Kadeck as they allow for not only decoding and encoding custom data formats but also the integration of additional (web) services.

Learn more about custom codecs in Kadeck: https://support.xeotek.com/hc/en-us/categories/360002602440-Custom-Codecs

## How to build
Codecs must be packaged as a Fat JAR and placed in the appropriate custom codecs directory. The directory is specified in the Kadeck settings and should only contain the Fat JAR(s).

### Preparation
If you need to add further dependencies to the project, define them as usual in the POM file.

>**Important**: If you use Kafka libraries (e.g. "kafka-clients"), declare them with the scope "provided" (see javax.json as an example). Otherwise, version conflicts may occur when loading the codecs.

More information about the Maven Assembly Plugin (for creating the Fat JAR), e.g. about excluding dependencies within other libraries can be found here: https://maven.apache.org/guides/mini/guide-assemblies.html.

### Create a Fat JAR
Run the Maven command with the target package to create a Fat JAR with the necessary dependencies.

`mvn clean package`

### Configure custom codec directory in KaDeck
Start Kadeck and enter the path to the created folder under Settings (Kadeck Desktop) or Administration/Settings (Kadeck Teams).

Codecs are automatically loaded and initialised when a connection is established (Apache Kafka or Amazon Kinesis).

## Debugging
Use the log4j logger - as in the example project - to generate log output.
Where to find the log file for KaDeck Desktop is described in this support article:
https://support.xeotek.com/hc/en-us/articles/360015591659-Log-file


_Copyright 2023 Xeotek GmbH - All rights reserved._
