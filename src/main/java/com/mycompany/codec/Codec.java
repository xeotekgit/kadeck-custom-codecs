// Copyright 2021 Xeotek GmbH - All rights reserved.
package com.mycompany.codec;

import com.xeotek.kadeck.codec.*;

import javax.json.Json; // We use PROVIDED scope to exclude this dependency from being packaged (see POM)
import javax.json.JsonValue; // We use PROVIDED scope to exclude this dependency from being packaged (see POM)
import java.util.ArrayList;
import java.util.Properties;

/**
 * Custom Codec Example for Xeotek KaDeck
 * <br/>Visit https://support.xeotek.com/hc/en-us/categories/360002602440-Custom-Codecs for more information.
 */
public class Codec implements RecordCodec {

    private static final org.apache.logging.log4j.Logger log  = org.apache.logging.log4j.LogManager.getLogger(Codec.class);
    private static ArrayList<String> historyCache;   // This is our shared state (for KaDeck Enterprise developers)
    private static final Object lock = new Object();

    @Override
    public void onInit(Properties config) throws DecoderInitialisationException {
        log.info("Hello from custom codec 'Codec'");

        synchronized (lock) {
            if (historyCache == null)
                historyCache = new ArrayList<>();
            historyCache.add(String.format("User has connected to %s - %s", config.getProperty(CodecConfig.SERVER_ID), config.getProperty(CodecConfig.SERVER_NAME))); // We access the details of the server the user is connected to - Note: SERVER_NAME might not be unique.
        }
    }

    private String tempHistory;

    @Override
    public void prepareDecoding(Properties config, String topic) {
        synchronized (lock) {
            tempHistory = historyCache.toString();  // We save the current historyCache state for faster access as preparation for the decoding process
        }
    }

    private JsonValue decode(byte[] bytes) throws DecodingException {
        // We ignore the original bytes in this example as we are returning static values.
        return Json.createValue(tempHistory);
        // A string decoder could look like this:
        // return Json.createValue(new String(bytes, StandardCharsets.UTF_8));
    }

    @Override
    public int priority() {
        return -1;  // -1 as we do not want this codec to be included in the auto-detection process. Use >10 otherwise.
    }

    @Override
    public String id() {
        return "Custom Codec Example";  // This name is shown to the user.
    }

    @Override
    public DecodedResult decodeValue(String topic, byte[] bytes) throws DecodingException {
        return new DecodedResult(decode(bytes));
    }

    @Override
    public DecodedResult decodeKey(String topic, byte[] bytes) throws DecodingException {
        return new DecodedResult(decode(bytes));
    }
}
