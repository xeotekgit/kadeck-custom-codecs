// Copyright 2021 Xeotek GmbH - All rights reserved.
package com.xeotek.kadeck.codec;

/**
 * Used to access configuration properties passed to the {@link RecordCodec} life-cycle methods and encoding methods.
 * @author Xeotek GmbH
 * @since 3.1.0
 */
public interface CodecConfig {
	long serialVersionUID = 145514833219322431L;

	/** Prefix for compatibility if configuration data is added later **/
	String prefix = "server." ;
	/** Bootstrap servers (comma-separated if applicable) 192.168.0.1:9092,192.168.0.2:9092  **/
	String SERVER_ADDRESS = prefix+"address";
	/** The name of the server - not unique **/
	String SERVER_NAME = prefix+"name";
	/** Unique server id as string **/
	String SERVER_ID = prefix+"id";
	/** Schema Registry address <host>:<port>  **/
	String SERVER_SCHEMAREGISTRY_ADDRESS = prefix+"schemaregistry.address";
	/** Security Truststore location  **/
	String SERVER_SECURITY_TRUSTSTORE_LOC = prefix+"security.truststore.loc";
	/** Security Truststore password  **/
	String SERVER_SECURITY_TRUSTSTORE_PASS = prefix+"security.truststore.pass";
	/** Security Keystore location  **/
	String SERVER_SECURITY_KEYSTORE_LOC = prefix+"security.keystore.loc";
	/** Security Keystore password  **/
	String SERVER_SECURITY_KEYSTORE_PASS = prefix+"security.keystore.pass";
	/** Security Keystore private password  **/
	String SERVER_SECURITY_KEYSTORE_PRIVATEPASS = prefix+"security.keystore.privatepass";
	/** Security protocol **/
	String SERVER_SECURITY_PROTOCOL = prefix+"security.protocol";
	/** SASL config in the format: loginModuleClass controlFlag (optionName=optionValue)*;  **/
	String SERVER_SASL_CONFIG = prefix+"sasl.config";
	/** SASL mechanism, e.g. PLAIN or GSSAPI **/
	String SERVER_SASL_MECHANISM = prefix+"sasl.mechanism";
	/** Schema Registry authentication protocol , e.g. BASIC or BEARER **/
	String SERVER_SCHEMAREGISTRY_PROTOCOL = prefix+"schemaregistry.protocol";
	/** User or bearer token depending on the protocol. **/
	String SERVER_SCHEMAREGISTRY_USERINFO = prefix+"schemaregistry.userinfo";
	/** Skip hostname verification. **/
	String SERVER_SCHEMAREGISTRY_SKIPHOSTNAME = prefix+"schemaregistry.skiphostname";
	/** HashMap<String,Object> **/
	String ADDITIONAL_META_MAP = prefix+"meta";
}
