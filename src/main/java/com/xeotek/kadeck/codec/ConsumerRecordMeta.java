package com.xeotek.kadeck.codec;

public interface ConsumerRecordMeta {
    long serialVersionUID = -8586192119294894121L;

    String prefix = "kafka.";

    String TOPIC = prefix+"topic";
    String PARTITION = prefix+"partition";
    String OFFSET = prefix+"offset";

    /**
     * Contains the last occurrence of all headers as a HashMap <String,byte[]>.
     */
    String HEADERS_MAP = prefix+"headers";

    String TIMESTAMP = prefix+"timestamp";
    String TIMESTAMP_TYPE = prefix+"timestamptype";
    String SERIALIZED_KEY_SIZE = prefix+"serializedkeysize";
    String SERIALIZED_VALUE_SIZE = prefix+"serializedvaluesize";
}
