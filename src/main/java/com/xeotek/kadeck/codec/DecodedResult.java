// Copyright 2021 Xeotek GmbH - All rights reserved.
package com.xeotek.kadeck.codec;

import javax.json.JsonValue;
import java.util.Map;

/**
 * The result of the {@link RecordCodec#decodeKey} and {@link RecordCodec#decodeValue} methods.<br/>
 * For compatibility a map with additional meta information can be passed optionally to the constructor which might be used in later versions.
 * @author Xeotek GmbH
 * @since 1.4.0
 */
public class DecodedResult {
	long serialVersionUID = -1360580815076863872L;

	private final Map<String, String> meta;
	private final JsonValue value;

	public DecodedResult(JsonValue value){
		this.value = value;
		this.meta = null;
	}

	public DecodedResult(JsonValue value, Map<String, String> meta){
		this.value = value;
		this.meta = meta;
	}

	public Map<String, String> getMeta(){
		return meta;
	}

	public JsonValue getValue(){
		return value;
	}
}
