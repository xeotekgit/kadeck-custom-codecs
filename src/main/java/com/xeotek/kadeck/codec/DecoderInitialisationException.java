// Copyright 2021 Xeotek GmbH - All rights reserved.
package com.xeotek.kadeck.codec;

/**
 * Exception when the codec could not be initialised after establishing a server connection.<br/>
 * Throw this with a meaningful message to be displayed to the user.
 * @author Xeotek GmbH
 * @since 1.4.0
 */
public class DecoderInitialisationException extends RuntimeException {
	private static final long serialVersionUID = -3099782430552507473L;

	public DecoderInitialisationException(String message, Throwable cause) {
		super(message, cause);
	}

	public DecoderInitialisationException(String message) {
		super(message);
	}

	public DecoderInitialisationException(Throwable cause) {
		super(cause);
	}

	public DecoderInitialisationException() {
		super();
	}

	/* avoid the expensive and useless stack trace for serialization exceptions */
	@Override
	public Throwable fillInStackTrace() {
		return this;
	}

}
