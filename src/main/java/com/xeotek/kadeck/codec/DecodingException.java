// Copyright 2021 Xeotek GmbH - All rights reserved.
package com.xeotek.kadeck.codec;

/**
 * Exception when decoding is not possible or any critical error has occurred that prevents the codec from decoding the record.<br/>
 * Also used during auto-detection to determine the applicable codec.
 * @author Xeotek GmbH
 * @since 1.4.0
 */
public class DecodingException extends RuntimeException {
	private static final long serialVersionUID = 7608331065390559993L;

	public DecodingException(String message, Throwable cause) {
		super(message, cause);
	}

	public DecodingException(String message) {
		super(message);
	}

	public DecodingException(Throwable cause) {
		super(cause);
	}

	public DecodingException() {
		super();
	}

	/* avoid the expensive and useless stack trace for serialization exceptions */
	@Override
	public Throwable fillInStackTrace() {
		return this;
	}
}
