// Copyright 2021 Xeotek GmbH - All rights reserved.
package com.xeotek.kadeck.codec;

import javax.json.JsonValue;
import java.util.Properties;


/**
 * When designing and implementing a custom codec, consider these important rules:<ul>
 * <li><u>Don't do any heavy work in the decoding methods</u> ({@link #decodeKey}/ {@link #decodeValue}); use the lifecycle methods {@link #onInit} or {@link #prepareDecoding} for preparation.</li>
 * <li><u>Fail early when decoding</u> - throw a {@link DecodingException} as soon as possible!<br/>If your codec is included in the auto-detection process, which you can control via {@link #priority}, the decode methods {@link #decodeKey}/ {@link #decodeValue} are receiving the first record of every consumption request in order to determine if the codec is applicable.</li>
 * <li>
 * <u>Keep track of your state.</u> A codec instance created per user when connecting to a server. Use static to share data across user instances.</li></ul>
 * <p><b>Life-Cycle:</b></p>
 * <p>
 *   <b>1.</b> When a connection to a server is established (a user has clicked "connect" on a server connection) the codecs are instantiated for this user and the  {@link #onInit} method is invoked.<br/>
 *   Decoding (Consumption): <br/>
 *   <b>2.</b> For every consumption request (a batch of records shall be consumed) the {@link #prepareDecoding} method is invoked.<br/>
 *   <b>3.</b> If the codec is included in the auto-detection process, the decoding methods ({@link #decodeKey} / {@link #decodeValue}) are invoked with the first record to determine if the codec is applicable.<br/>
 *   <b>4.</b> If no {@link DecodingException} was thrown during the auto-detection process, the decoding methods ({@link #decodeKey} / {@link #decodeValue}) are invoked for every record of the consumption request.<br/>
 *   Encoding (Ingestion):<br/>
 *   <b>2.</b> For every ingestion request (a batch of records shall be ingested) the {@link #prepareEncoding} method is invoked.<br/>
 *   <b>3.</b> The encoding methods {@link #encodeKey} and {@link #encodeValue} are being called for every record.<br/><br/>
 *   <u>Note:</u> To include the codec in the ingestion dialog, overwrite {@link #canEncode()} to return true.
 * </p><br/>
 * <b>Auto-Detection Process:</b><br/>

 * <p>The decoding methods of the first codec (the one with the highest priority) are invoked with the first record of the consumption request. If the record cannot be decoded by the codec, the corresponding decoding method must throw an {@link DecodingException}. The record is then passed on to the next codec. If the codec is applicable for the decoding, the decoded value must be returned.</p>

 * <b>Read the descriptions of every interface method for more information.</b><br/>
 * @author Xeotek GmbH
 * @since 1.4.0
 */
public interface RecordCodec {
	long serialVersionUID = -2199896213096792594L;

	int DEFAULT_PRIORITY = 10;
	int SIMPLE_STRING_DECODER_PRIORITY = 0;

	/**
	 * <b>Auto-Detection:</b><br/>
	 * The priority is used to determine in which order the codecs should get the ability to determine if they are applicable of processing the records. <br/>
	 * Default priority is 10. The higher the priority, the earlier the decoder gets the value. Use a negative value to exclude decoder from auto-detection.
	 * <br/>
	 * To avoid collisions with built-in decoders, a priority higher than 10 is recommended.
	 * @return the priority of your decoder.
	 */
	int priority();

	/**
	 * Used for the UI.
	 * @return a string representation of the type that your decoder decodes. E.g. "Int", "Avro", ...
	 */
	String id();


	/**
	 * If set to true, the codec will be shown in the ingestion dialog.
	 * <br/>
	 * @return true, if the codec can be used to encode key and value for data ingestion.
	 */
	default boolean canEncode(){
		return false;
	}

	/**
	 * Gets called when a new connection to a server is made.
	 * <br/>
	 * Use this method for long running tasks. You don't need to do anything if your codec does not need to do any preparation before decoding/encoding (such as making connections and fetching data from somewhere else and building a cache).<br/>
	 * <br/><p>
	 *     <u>In case of an error:</u> If you want to notify the user that your codec could not be initialised correctly, throw a {@link DecoderInitialisationException} with the respective error message.
	 * </p><br/>
	 * <u>Example usage:</u><br/>
	 * Avro Decoder uses this method to pre-fetch all schemas from the server and to store it in-memory, so the data is available prior decoding / encoding.<br/>
	 * <br/>
	 * It is a good idea to use the {@link CodecConfig#SERVER_ID} when building a static cache per server. On KaDeck Enterprise the (static) cache will be shared between users.
	 * @param config The server a user has connected to.  See {@link CodecConfig}.
	 */
	default void onInit(Properties config) throws DecoderInitialisationException{
	}

	/**
	 * <p>Only called prior decoding.</p>
	 * Gets called prior starting with the decoding process.
	 * <br/>
	 * Use this method to do one time work prior batch processing (=subsequent calls to the decoding methods).<br/>
	 * <u>Example:</u>
	 * <br/>
	 * Avro Codec uses this to get the current schema for the topic to be decoded from the cache and stores it for the decode - methods to be used later.
	 *
	 * @param config The server of the upcoming decoding request.
	 * @param topic        The topic of the upcoming decoding request.
	 */
	default void prepareDecoding(Properties config, String topic) {
	}

	/**
	 * <p>Only called prior encoding.</p>
	 * Gets called prior starting with the encoding process.
	 * <br/>
	 * Use this method to do one time work prior batch processing (=subsequent calls to the encoding methods).<br/>
	 * <u>Example:</u>
	 * <br/>
	 * Avro Codec uses this to get the current schema for the records to be encoded from the cache and stores it for the encode - methods to be used later.
	 *
	 * @param config The server of the upcoming decoding request. See {@link CodecConfig}.
	 * @param topic        The topic of the upcoming decoding request.
	 */
	default void prepareEncoding(Properties config, String topic) {
	}


	/**
	 * Fail early (throw {@link DecodingException}) in order to keep processing time as short as possible.
	 *
	 * @param topic The topic of the consumption request.
	 * @param bytes The bytes of the record to be decoded.
	 * @return DecodedResult
	 * @throws DecodingException if decoding is not possible.
	 */
	DecodedResult decodeValue(String topic, byte[] bytes) throws DecodingException;

	/**
	 * Fail early (throw {@link DecodingException}) in order to keep processing time as short as possible.
	 *
	 * @param topic The topic of the consumption request.
	 * @param bytes The bytes of the record to be decoded.
	 * @return DecodedResult
	 * @throws DecodingException if decoding is not possible.
	 */
	default DecodedResult decodeValue(String topic, byte[] bytes, Properties consumerRecordMeta) throws DecodingException {
		return this.decodeValue(topic, bytes);
	}

	/**
	 * Fail early (throw {@link DecodingException}) in order to keep processing time as short as possible.
	 *
	 * @param topic The topic of the consumption request.
	 * @param bytes The bytes of the record to be decoded.
	 * @return DecodedResult
	 * @throws DecodingException if decoding is not possible.
	 */
	DecodedResult decodeKey(String topic, byte[] bytes) throws DecodingException;

	/**
	 * Fail early (throw {@link DecodingException}) in order to keep processing time as short as possible.
	 *
	 * @param topic The topic of the consumption request.
	 * @param bytes The bytes of the record to be decoded.
	 * @param consumerRecordMeta Meta information about the consumer record; see {@link ConsumerRecordMeta}
	 * @return DecodedResult
	 * @throws DecodingException if decoding is not possible.
	 */
	default DecodedResult decodeKey(String topic, byte[] bytes, Properties consumerRecordMeta) throws DecodingException{
		return this.decodeKey(topic,bytes);
	}

	/**
	 * Encode a JsonValue to the codec's type and return the byte[] representation.
	 * @param topic The topic to send the record to.
	 * @param partition The partition to send the record to.
	 * @param value The value to decode as JsonValue (can be of any {@link javax.json.JsonValue.ValueType})
	 * @param meta Optional meta data for your codec.
	 * @return the byte[] representation of the encoded value
	 */
	default byte[] encodeValue(String topic, int partition, JsonValue value, Properties meta){
		return encodeValue(topic, value, meta);
	}

	/**
	 * Encode a JsonValue to the codec's type and return the byte[] representation.
	 * @param topic The topic to send the record to.
	 * @param partition The partition to send the record to.
	 * @param value he value to decode as JsonValue (can be of any {@link javax.json.JsonValue.ValueType})
	 * @param meta Optional meta data for your codec.
	 * @return the byte[] representation of the encoded key
	 */
	default byte[] encodeKey(String topic, int partition, JsonValue value, Properties meta){
		return encodeKey(topic, value, meta);
	}

	/**
	 * Encode a JsonValue to the codec's type and return the byte[] representation.
	 * @param topic The topic to send the record to.
	 * @param value The value to decode as JsonValue (can be of any {@link javax.json.JsonValue.ValueType})
	 * @param meta Optional meta data for your codec.
	 * @return the byte[] representation of the encoded value
	 */
	default byte[] encodeValue(String topic, JsonValue value, Properties meta){
		return null;
	}

	/**
	 * Encode a JsonValue to the codec's type and return the byte[] representation.
	 * @param topic The topic to send the record to.
	 * @param value he value to decode as JsonValue (can be of any {@link javax.json.JsonValue.ValueType})
	 * @param meta Optional meta data for your codec.
	 * @return the byte[] representation of the encoded key
	 */
	default byte[] encodeKey(String topic, JsonValue value, Properties meta){
		return null;
	}

}
